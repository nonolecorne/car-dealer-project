from fastapi import FastAPI
from pydantic import BaseModel
from enum import Enum

class Brand(Enum):
    Renault = 1
    Peugeot = 2
    Citroen = 3
    Ford = 4
    Volkswagen = 5
    BMW = 6
    Mercedes = 7
    Audi = 8
    Porsche = 9
    Ferrari = 10

class Engine(Enum):
    Diesel = 1
    Essence = 2
    Electrique = 3
    Hybride = 4


class Color(Enum):
    Red = 1
    Blue = 2
    Green = 3
    Yellow = 4
    Black = 5
    White = 6
    Grey = 7
    Orange = 8
    Brown = 9
    Purple = 10

class Options(Enum):
    air_conditioning = 1
    cruise_control = 2
    electric_windows = 3
    electric_mirrors = 4
    electric_seats = 5
    electric_sunroof = 6
    electric_handbrake = 7
    electric_steering_wheel = 8
    electric_parking_brake = 9
    electric_trunk = 10

class Status(Enum):
    New = 1
    Used = 2
    Demo = 3   

class CarType(BaseModel):
    id: int
    brand: Brand
    doors_nb: int
    engine: Engine 

class Car(BaseModel):
    id: int
    cartype_nb: int
    km: int
    color: Color
    sale_price: int
    buying_price: int
    options: Options
    status: Status
    year_of_construction: int
    parking_place: int
    date_year: int
    date_livrable_review: str
    sales_employee: int
    preview_owner: int
    new_owner: int

carType = [
    {"id": 1, "brand": "Renault", "doors_nb": 5, "engine": "Essence"},
    {"id": 2, "brand": "Peugeot", "doors_nb": 3, "engine": "Diesel"},
    {"id": 3, "brand": "Citroen", "doors_nb": 5, "engine": "Electrique"},
    {"id": 4, "brand": "Ford", "doors_nb": 5, "engine": "Hybride"},
    {"id": 5, "brand": "Volkswagen", "doors_nb": 5, "engine": "Diesel"},
    {"id": 6, "brand": "BMW", "doors_nb": 3, "engine": "Diesel"},
    {"id": 7, "brand": "Mercedes", "doors_nb": 5, "engine": "Diesel"},
    {"id": 8, "brand": "Audi", "doors_nb": 5, "engine": "Diesel"},
    {"id": 9, "brand": "Porsche", "doors_nb": 3, "engine": "Diesel"},
    {"id": 10, "brand": "Ferrari", "doors_nb": 3, "engine": "Diesel"},
]

cars = [
    {"id": 1, "cartype_nb": 1, "km": 10000, "color": Color.Blue, "sale_price": 10000, "buying_price": 10000, "options": Options.air_conditioning, "status": Status.New, "year_of_construction": 2021, "parking_place": 1, "date_year": 2021, "date_livrable_review": "2021-01-01", "sales_employee": 1, "preview_owner": 1, "new_owner": 1},
    {"id": 2, "cartype_nb": 2, "km": 10000, "color": Color.Black, "sale_price": 10000, "buying_price": 10000, "options": Options.air_conditioning, "status": Status.New, "year_of_construction": 2021, "parking_place": 1, "date_year": 2021, "date_livrable_review": "2021-01-01", "sales_employee": 1, "preview_owner": 1, "new_owner": 1},
    {"id": 3, "cartype_nb": 3, "km": 10000, "color": Color.Brown, "sale_price": 10000, "buying_price": 10000, "options": Options.electric_handbrake, "status": Status.New, "year_of_construction": 2021, "parking_place": 1, "date_year": 2021, "date_livrable_review": "2021-01-01", "sales_employee": 1, "preview_owner": 1, "new_owner": 1},
    {"id": 4, "cartype_nb": 4, "km": 10000, "color": Color.Grey, "sale_price": 10000, "buying_price": 10000, "options": Options.electric_mirrors, "status": Status.New, "year_of_construction": 2021, "parking_place": 1, "date_year": 2021, "date_livrable_review": "2021-01-01", "sales_employee": 1, "preview_owner": 1, "new_owner": 1},
    {"id": 5, "cartype_nb": 5, "km": 10000, "color": Color.Blue, "sale_price": 10000, "buying_price": 10000, "options": Options.cruise_control, "status": Status.New, "year_of_construction": 2021, "parking_place": 1, "date_year": 2021, "date_livrable_review": "2021-01-01", "sales_employee": 1, "preview_owner": 1, "new_owner": 1},
]