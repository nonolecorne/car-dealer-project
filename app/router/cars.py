##Systeme Imports

##Librairy Imports
from fastapi import APIRouter, status
from fastapi.responses import Response

##Local Imports
from model.cars import Car, cars

router=APIRouter()

@router.get("/cars", responses={status.HTTP_204_NO_CONTENT: {}})
async def get_all_cars() -> list[Car]:
    if len(cars) == 0:
        return Response(status_code=status.HTTP_204_NO_CONTENT)
    return cars
