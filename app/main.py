'''systeme / librairie / local'''
from fastapi import FastAPI
from pydantic import BaseModel

from router import cars

app = FastAPI()


users = [
    {"id": 1, "name": "Devaux", "surname" : "Léo", "email": "leod1@gmail.com", "personnel_hash": "Leod125", "tel": "0654789566", "newsletter":1, "is_client": 1},
    {"id": 2, "name": "Guyart", "surname" : "yanis", "email": "hguyart@gmail.com", "personnel_hash": "Yanisguyart", "tel": "0654789566", "newsletter":1, "is_client": 1},
    {"id": 3, "name": "Pierre", "surname" : "Louis", "email": "pl454@gmail.com", "personnel_hash": "PierreL", "tel": "0654789566", "newsletter":0, "is_client": 0},
    {"id": 4, "name": "Jacques", "surname" : "Paul", "email": "pauljacques45@gmail.com", "personnel_hash": "PaulJ", "tel": "0654789566", "newsletter":1, "is_client": 0},
]

@app.get("/")
async def say_hello():
    """Ceci est une fonction qui dit bonjour oulala
    """
    return "bonjour"


class User(BaseModel):
    id: int
    name: str
    surname: str
    email: str
    personnel_hash: str
    tel: str
    newsletter: bool
    is_client: bool



@app.get("/users", tags=["users"], response_model_exclude_unset=True)
async def get_all_users() -> list[User]:
    return users

@app.get("/users/search", tags=["users"])
async def search_users(name : str):
    return list(filter(lambda x: x["name"] == name, users))

@app.get("/users/{user_id}", tags=["users"])
async def get_user_by_id(user_id: int, name: str | None = None):
    filtred_list = list(filter(lambda x: x["id"] == user_id, users))
    if name is not None :
        filtred_list = list(filter(lambda x: x["name"] == name, users))
    return filtred_list

@app.post("/users", tags=["users"])
async def create_user(user: User):
    users.append(user)
    return user

@app.delete("/users/{user_id}")
async def delete_user(user_id: int):
    users.remove(list(filter(lambda x: x["id"] == user_id, users))[0])
    return {"message": "user deleted"}

app.include_router(cars.router)
